# MPLAB IDE generated this makefile for use with GNU make.
# Project: uart.mcp
# Date: Mon Feb 10 15:03:30 2014

AS = MPASMWIN.exe
CC = mcc18.exe
LD = mplink.exe
AR = mplib.exe
RM = rm

uart.cof : uart.o shell.o commands.o sensors.o
	$(LD) /p18F4550 "uart.o" "shell.o" "commands.o" "sensors.o" /u_CRUNTIME /z__MPLAB_BUILD=1 /o"uart.cof" /M"uart.map" /W

uart.o : uart.c C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/stdio.h C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/stdlib.h C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/delays.h headers/shell.h headers/sensors.h uart.c C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/stdarg.h C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/stddef.h C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/p18cxxx.h C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/p18f4550.h
	$(CC) -p=18F4550 "uart.c" -fo="uart.o" -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-

shell.o : shell/shell.c C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/stdio.h C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/stdlib.h C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/delays.h C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/usart.h headers/commands.h headers/shell.h shell/shell.c C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/stdarg.h C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/stddef.h C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/p18cxxx.h C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/p18f4550.h C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/pconfig.h
	$(CC) -p=18F4550 "shell\shell.c" -fo="shell.o" -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-

commands.o : shell/commands.c C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/stdio.h C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/stdlib.h C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/delays.h C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/adc.h headers/shell.h headers/sensors.h shell/commands.c C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/stdarg.h C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/stddef.h C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/p18cxxx.h C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/p18f4550.h C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/pconfig.h
	$(CC) -p=18F4550 "shell\commands.c" -fo="commands.o" -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-

sensors.o : shell/sensors.c C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/stdio.h C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/stdlib.h C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/delays.h C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/string.h C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/adc.h C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/math.h shell/sensors.c C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/stdarg.h C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/stddef.h C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/p18cxxx.h C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/p18f4550.h C:/Program\ Files\ (x86)/Microchip/mplabc18/v3.46/h/pconfig.h
	$(CC) -p=18F4550 "shell\sensors.c" -fo="sensors.o" -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-

clean : 
	$(RM) "uart.o" "shell.o" "commands.o" "sensors.o" "uart.cof" "uart.hex"

