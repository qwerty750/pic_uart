#ifndef __SENSORS_H
#define __SENSORS_H

extern int init_adc();

extern int readFromSensors();

extern int getData(unsigned char sensor, char *data);

#endif