#ifndef __SHELL_H
#define __SHELL_H

//Pushes a string into output buffer (for messages or errors from PIC).
char out_pushString(char *message);

//Pops one character from output buffer. Is used in "uart.c" file.
void out_pop();

//Processes pressings of keyboard. Is used in "uart.c" file.
void shell_key();

#endif