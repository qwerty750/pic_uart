#ifndef __COMMANDS_H
#define __COMMANDS_H

//Array of command's structures that contains command's names and references to functions that are connected to commands.
extern struct {
	char name[10];
	void (*command)(unsigned char*);
} commands[];

#endif