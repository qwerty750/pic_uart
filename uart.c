#include <stdio.h>
#include <stdlib.h>
#include <p18cxxx.h>
#include <delays.h>
#include "headers\shell.h"
#include "headers\sensors.h"

//Configure UART module
void init_uart()
{
	TRISCbits.RC6 = 1;
	TRISCbits.RC7 = 1;
	SPBRGH = 0;
	SPBRG = 31;
	BAUDCONbits.BRG16 = 0;
	TXSTAbits.SYNC = 0;
	RCSTAbits.SPEN = 1;
	TXSTAbits.TXEN = 1;
	RCSTAbits.CREN = 1;
}

//Configure another stuff
void init()
{
	TRISB = 0;
	PORTB = 0;
	TRISA = 255;
	PORTA = 0;
	PORTB = 255;
}

void mainLoop()
{
	while (1)
	{
		out_pop();
		shell_key();
		readFromSensors();
	}
}

void main()
{	
	init_adc();
	init_uart();
	init();
	mainLoop();
}