#include <stdio.h>
#include <stdlib.h>
#include <p18cxxx.h>
#include <delays.h>
#include <adc.h>
#include "headers\shell.h"
#include "headers\sensors.h"

/*
Each command must be pre-declared.
Also they should be added into array of command's structs.
Each command should be declared as "void command_name(unsigned char *parsedCommand[])"
*/

static unsigned char porta = 0;
static unsigned char portb = 0;
static unsigned char portc = 0;
static unsigned char portd = 0;

//Pre-declaration of commands
static void com_onCommand(unsigned char *parsedCommand[]);
static void com_sensor(unsigned char *parsedComsmand[]);
static void com_offCommand(unsigned char *parsedCommand[]);
static void com_help(unsigned char *parsedCommand[]);


//Array of command's structures contains name of command that will be used in terminal for executing command and reference to function that contains command.
struct {
	char name[10];
	void (*command)(unsigned char**);
} commands[] = {
	{"off", com_offCommand},
	{"on", com_onCommand},
	{"voltage", com_sensor},
	{"help", com_help}, 
	{"", 0}
};

 //Example of command
static void com_onCommand(unsigned char *parsedCommand[])
{	
	char msg[30] = "\nSyntax error\n";
	char success[30] = "\nSuccess\n";
	if (parsedCommand[3] == 0 && parsedCommand[2] != 0 && parsedCommand[1] != 0)
	{
		unsigned char bit = atoi((parsedCommand[2]));
		unsigned char port = parsedCommand[1][0];
		switch (port)
		{
			case 'a':
				porta |= 1 << bit;
				PORTA = porta;
				break;
			case 'b':
				portb |= 1 << bit;
				PORTB = portb;
				break;
			case 'c':
				portc |= 1 << bit;
				PORTC = portc;
				break;
			case 'd':
				portd |= 1 << bit;
				PORTD = portd;
				break;
		}
		out_pushString(success);
	}
	else
		out_pushString(msg);
}

static void com_offCommand(unsigned char *parsedCommand[])
{
	char msg[30] = "\nSyntax error\n";
	char success[30] = "\nSuccess\n";
	if (parsedCommand[3] == 0 && parsedCommand[2] != 0 && parsedCommand[1] != 0)
	{
		unsigned char bit = atoi((parsedCommand[2]));
		unsigned char port = parsedCommand[1][0];
		switch (port)
		{
			case 'a':
				porta &= ~(1 << bit);
				PORTA = porta;
				break;
			case 'b':
				portb &= ~(1 << bit);
				PORTB = portb;
				break;
			case 'c':
				portc &= ~(1 << bit);
				PORTC = portc;
				break;
			case 'd':
				portc &= ~(1 << bit);
				PORTD = portd;
				break;
		}
		out_pushString(success);
	}
	else
		out_pushString(msg);
}

static void com_sensor(unsigned char *parsedCommand[])
{
	char res[20];
	unsigned char sens = atoi(parsedCommand[1]);
	char buf[4] = {2, sens + '0', 'x', 0};
	getData(sens, res);
	out_pushString(buf);	
	out_pushString(res);	
}

static void com_help(unsigned char *parsedCommand[])
{
	char n[] = "\n";
	int i;
	for (i = 0; commands[i].command != 0; i++)
	{
		out_pushString(n);
		out_pushString(commands[i].name);		
	}
}


