#include <stdio.h>
#include <stdlib.h>
#include <p18cxxx.h>
#include <delays.h>
#include <usart.h>
#include "headers\commands.h"
#include "headers\shell.h"

//Sizes of input & buffers
#define IN_SIZE 101
#define OUT_SIZE 110

//Max command's parameter's number (with command)
#define MAX_PARAM_NUM 5

//Input buffer
static struct {
	char head;
	unsigned char buf[IN_SIZE];
} in = {0};

//Output buffer
static struct {
	unsigned char buf[OUT_SIZE];
	int head;
	int tail;
} out = {0};

//Pushes a string into output buffer
char out_pushString(char *message)
{
	int i;
	int length = strlen(message);
	for (i = 0; message[i] != '\0'; i++)
	{
		if ((out.head + 1) % OUT_SIZE == out.tail)
			return length - i;
		out.buf[out.head] = message[i];
		out.head = (out.head + 1) % OUT_SIZE;
	}
	return 0;
}

//Pops one character from out buffer
void out_pop()
{
	if (out.tail == out.head)
		return;
	if (!BusyUSART())
	{
		putcUSART(out.buf[out.tail]);
		out.tail = (out.tail + 1) % OUT_SIZE;
	}
}

//Clears input buffer	
static void in_clear()
{
	in.head = 0;
}

//Deletes one character from input buffer
static void in_deleteChar()
{
	if (in.head > 0)
		in.head--;
}

//Pushes a character into input buffer
static char in_push(unsigned char key)
{
	if (in.head == IN_SIZE)
		return -1;
	in.buf[in.head++] = key;
	return 0;
}

/*
Divides input buffer into command and parameters
Command must start from first character of buffer
*/
static void shell_parseCommand(unsigned char *command[])
{
	char j = 0;
	unsigned char i;
	for (i = 0; i < MAX_PARAM_NUM; i++)
		command[i] = 0;
	command[j] = in.buf;
	for (i = 1; i < in.head; i++)
		if ((in.buf[i - 1] == '\0') && (in.buf[i] != '\0'))
			command[++j] = &in.buf[i];
}

/*
Looks for command in array of command's structure
If the command is found function will return index of command in array.
If the command is not foun function will return -1
*/
static char shell_detectCommand(unsigned char *parsedCommand[])
{
	unsigned char i, j;
	shell_parseCommand(parsedCommand);
	for (i = 0; commands[i].command != 0; i++)
	{
		if (!strcmp(commands[i].name, parsedCommand[0]))
			return i;
	}
	return -1;	
}

//Runs the command from array of command's structure if it is found
static void shell_choseCommand()
{	
	char msg[20] = "\nCommand not found\n";
	unsigned char *parsedCommand[MAX_PARAM_NUM];
	char command = shell_detectCommand(parsedCommand);
 	if (command >= 0)
		commands[command].command(parsedCommand);
	else
		out_pushString(msg);
}

//Processes pressings of keyboard
void shell_key()
{
	if (DataRdyUSART())
	{
		char key = getcUSART();
		switch (key)
		{
			/*case 13:	
				in_push(0);			
				shell_choseCommand();
				in_clear();
				break;*/
			case '\n':
				in_push(0);					
				shell_choseCommand();
				in_clear();
				break;
			/*case 0:
				in_push(0);			
				shell_choseCommand();
				in_clear();
				break;*/
			case ' ':
				in_push(0);
				putcUSART(key);
				break;
			/*case 8:
				in_push(0);
				putcUSART(key);
				break;*/
			/*case 8:
				putcUSART(key);
				in_deleteChar();
				break;*/
			case 255:
				break;
			default:
				//putcUSART(key);
				in_push(key);
		}
	}
}
