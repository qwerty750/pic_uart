#include <stdio.h>
#include <stdlib.h>
#include <p18cxxx.h>
#include <delays.h>
#include <string.h>
#include <adc.h>
#include <math.h>

#define SENSORS_COUNT 2
#define REF_VOLTAGE 5.04

#define BUF_SIZE 10

#define HYGRO_POS 0
#define BARO_POS 1

struct struct_sensor{
	int* buf;
	int position;
	char head;
	void (*command)(struct struct_sensor, char*);
};


void push(struct struct_sensor* sensor) {
	sensor->buf[sensor->head] = getADC(sensor->position);
	sensor->head = (sensor->head + 1) % BUF_SIZE;
}

int averaging(struct struct_sensor sensor) {
	int i;
	int sum = 0;
	int n = 0;
	for (i = 0; i < BUF_SIZE; i++)
		if (sensor.buf[i] != -1)
		{
			n++;
			sum += sensor.buf[i];
		}
	sum /= n;
	return sum;
}

float digitToVoltage(struct struct_sensor sensor) {
	return ((averaging(sensor) * REF_VOLTAGE) / 1024.0);
}

static int hygrometer[BUF_SIZE];
static int barometer[BUF_SIZE];


static void fn_hygrometer(struct struct_sensor sensor, char* data)
{
	double res = digitToVoltage(sensor);
	int r = (res - 0.8) * 25 / 0.8;
	sprintf(data, "\n%d\n", r);
}

static void fn_barometer(struct struct_sensor sensor, char* data)
{
	double res = ((averaging(sensor) * REF_VOLTAGE) / 1024.0);
	int d1 = floor(res);
	int d2 = (res - d1) * 100;
	//int r = (res - 0.8) * 25 / 0.8 * 100;
	sprintf(data, "\n%d.0%d\n", d1, d2);
}


struct struct_sensor sensors[] = {
	{hygrometer, HYGRO_POS, 0, fn_hygrometer},
	{barometer, BARO_POS, 0, fn_barometer}
};


void init_sensors() {
	int i, j;
	for (i = 0; i < SENSORS_COUNT; i++)
		for (j = 0; j < BUF_SIZE; j++)
			sensors[i].buf[j] = -1;
}

int init_adc() {
	ADCON1bits.PCFG = SENSORS_COUNT;
	ADCON2 = 0b10111110;
	init_sensors();
}
		
int getADC(int sensor) {
	ADCON0bits.CHS = sensor; 
	ADCON0bits.ADON = 1;
	ADCON0bits.GO = 1;
	while (BusyADC()) ;
	return (ADRESH * 256 + ADRESL);
}


int readFromSensors() {
	static unsigned int delay = 0;
	if (delay == 0) {
		int i;
		for (i = 0; i < SENSORS_COUNT; i++)
			push(&(sensors[i]));
	}
	delay = (delay + 1) % 999999999;
}

int getData(unsigned char sensor, char *data) {
	sensors[sensor].command(sensors[sensor], data);	
}